package com.haladyj.SS17RS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ss17RsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ss17RsApplication.class, args);
	}

}
