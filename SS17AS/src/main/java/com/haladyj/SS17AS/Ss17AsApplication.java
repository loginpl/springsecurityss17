package com.haladyj.SS17AS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ss17AsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ss17AsApplication.class, args);
	}

}
